**Packet sniffer used to catch ICMP packet on lo interface and create a copy of it**

To run the program you need opensource library named **libpcap**. Installing **libpcap** and **valgrind** (debugging tool used especially to check memory leaks) packets is part of **run.sh** script (apt-get command, Ubuntu 18.04) so you don't have to install them separately.

---

## Compilation and running

1. Clone repository.
2. Go to icotera_packet_sniffer directory.
3. To build and run the program use **sudo ./run.sh**
4. Set default route through "lo" interface. If you do not want to change default route, You can add **-I lo** option to ping command.
5. Open two terminals and run **sudo tcpdump -i lo icmp** to check incoming ICMP packets on "lo" interface and **ping -c 1 8.8.8.8** to send packet on "lo" interface.
6. To cleanup the solution go to "src" directory and run **make clean** command.