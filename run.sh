#!/bin/bash

# Check if libpcap-dev package is installed and if not then install it.
pkg='libpcap-dev'
if ! dpkg -s $pkg >/dev/null 2>&1; then
	apt-get --assume-yes install $pkg
fi

# Check if valgrind package is installed and if not then install it.
pkg='valgrind'
if ! dpkg -s $pkg >/dev/null 2>&1; then
	apt-get --assume-yes install $pkg
fi

# Run build and solution
function run_solution() {
	cd $(pwd)/src
	make
	./sniffer
}

run_solution
	
