#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pcap.h>

/* Sniffer app errorcodes: 0 - if success, -1 - if an error occures */
#define APP_OK 0
#define APP_ERR -1

/* Name of device to be sniffed */
#define DEV_NAME "lo"
/* Protocol type */
#define PROTOCOL "icmp"
/* Maximum amount of data to be captured */
#define SNAPLEN 65536
/* Promisc mode: 1 - promisc mode on, 0 - promisc mode off */
#define PROMISC_ON 1
#define PROMISC_OFF 0
/* Timeout in miliseconds */
#define TIMEOUT 0
/* Number of packets */
#define PACKET_CNT -1

/* Function used to copy packets - implemented as a handler to pcap_dispatch() function */
void packet_handler(u_char *, const struct pcap_pkthdr *, const u_char *);

int main()
{
	/* Device handler for sniffing */
	pcap_t *handle;
	/* Buffer to contain error message */
	char buf[PCAP_ERRBUF_SIZE];
	/* Return value in case of an error */
	int err;
	/* IP address */
	bpf_u_int32 ip;
	/* Network mask */
	bpf_u_int32 mask;
	/* Network device filter used to set for example packet type */
	struct bpf_program filter;

	/* Get IP address and network mask for "lo" device */
	err = pcap_lookupnet(DEV_NAME, &ip, &mask, buf);
	if (err < 0) {
		fprintf(stderr, "Couldn't get information about device \"%s\", err = %d\n", DEV_NAME, err);
		return APP_ERR;
	}

	/* Open "lo" device for sniffing */
	handle = pcap_open_live(DEV_NAME, SNAPLEN, PROMISC_ON, TIMEOUT, buf);
	if (!handle) {
		fprintf(stderr, "Couldn't open device \"%s\" for sniffing:\n%s\n", DEV_NAME, buf);
		return APP_ERR;
	}

	/* Compile packet filter before applying it */
	err = pcap_compile(handle, &filter, PROTOCOL, 0, ip);
	if (err < 0) {
		fprintf(stderr, "Couldn't compile packet filter, err = %d\n", err);
		return APP_ERR;
	}

	/* Apply packet filter */
	err = pcap_setfilter(handle, &filter);
	if (err < 0) {
		fprintf(stderr, "Couldn't apply packet filter, err = %d\n", err);
		return APP_ERR;
	}

	/* Sniff incoming packets on "lo" device */
	pcap_dispatch(handle, PACKET_CNT, packet_handler, (u_char *)handle);

	/* Free memory used by bpf_program structure */
	pcap_freecode(&filter);

	/* Close "lo" device handler */
	pcap_close(handle);
	
	return APP_OK;	
}

void packet_handler(u_char *arg, const struct pcap_pkthdr *header, const u_char *buffer)
{
	/* Get "lo" device handler */
	pcap_t *handle = (pcap_t *)arg;

	/* Send a copy of handled packet */
	pcap_inject(handle, buffer, header->len);
}
